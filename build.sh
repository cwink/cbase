#!/bin/sh

set -e

ccmd="gcc"
cflags="-Iinc -pedantic-errors -std=c99 -Wall -Wconversion -Wdouble-promotion -Wextra -xc"
cflagsdbg="-fsanitize=undefined -fsanitize-trap -g3 -O0"
cflagsrel="-Os"
checkcmd="cppcheck"
checkflags="--enable=all -Iinc --library=posix --suppress=missingIncludeSystem --std=c99"
ldflags=""
ldflagsdbg=""
ldflagsrel="-s -Wl,--gc-sections"
libs=""
project="cbase"
tagcmd="ctags"
tagflags="-aR --fields=+S"
tagincs="bin src inc"

clean()
{
	rm -f bin/*.x obj/*.o
}

configure()
{
	cat <<EOF >"config.mk"
CC      = ${ccmd}
CFLAGS  = ${cflags}
LDFLAGS = ${ldflags}
LIBS    = ${libs}
EOF
	{
		find bin/* -prune -type f -name "*.c" 2>"/dev/null" || true
		find src/* -prune -type f -name "*.c" 2>"/dev/null" || true
	} | ./gensrcmk.awk >"source.mk"
}

lint()
{
	# shellcheck disable=SC2086
	${checkcmd} --language=c ${checkflags} .
}

tags()
{
	# shellcheck disable=SC2086
	${tagcmd} ${tagflags} ${tagincs}
}

usage()
{
	cat <<EOF
Usage: ./build.sh [-d] [-h] [command]

	Script to manage and build the ${project} project.

	options:

		-d	Enable debugging options during configuration.
		-h	Show this help menu.

	commands:

		clean		Clean all build files.
		configure	Configure the project.
		lint		Perform linting on the project using ${checkcmd}.
		tags		Generate tags file for project using ${tagcmd}.
EOF
}

dbgopt=0

while getopts "dh" opt; do
	case "${opt}" in
	"d")
		dbgopt=1
		;;
	"?")
		printf "\n"
		usage
		exit 1
		;;
	*)
		usage
		exit 0
		;;
	esac
done

shift $((OPTIND - 1))

cmd="$(echo "${1}" | tr -d "[:space:]" | tr "[:upper:]" "[:lower:]")"

if [ "${dbgopt}" -eq 1 ]; then
	cflags="${cflags} ${cflagsdbg}"
	ldflags="${ldflags} ${ldflagsdbg}"
else
	cflags="${cflags} ${cflagsrel}"
	ldflags="${ldflags} ${ldflagsrel}"
fi

case "${cmd}" in
"clean")
	clean
	;;
"configure")
	configure
	;;
"lint")
	lint
	;;
"tags")
	tags
	;;
"")
	usage
	;;
*)
	printf "%s: illegal command -- %s\n\n" "${0}" "${cmd}" 1>&2
	usage
	exit 1
	;;
esac
