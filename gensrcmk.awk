#!/bin/awk -f

BEGIN {
	bcnt = 0
	scnt = 0
}

match($0, /\/[^\/]*$/) {
	obj = substr($0, RSTART + 1, RLENGTH)
	sub(/\.c$/, ".o", obj)
	obj = sprintf("obj/%s", obj)
}

/^bin/ {
	exe = substr($0, RSTART + 1, RLENGTH)
	sub(/\.c$/, ".x", exe)
	bexes[bcnt] = sprintf("bin/%s", exe)
	bfiles[bcnt] = $0
	bobjs[bcnt] = obj
	++bcnt
}

/^src/ {
	sfiles[scnt] = $0
	sobjs[scnt] = obj
	++scnt
}

END {
	for (i in sfiles) {
		if (i > 0) {
			objs = objs " "
		}
		objs = objs sobjs[i]
	}

	for (i in bfiles) {
		printf("%s: %s %s\n", bexes[i], bobjs[i], objs)
		printf("\t$(CC) $(LDFLAGS) -o $(@) $(^) $(LIBS)\n\n")
	}

	for (i in bfiles) {
		printf("%s: %s\n", bobjs[i], bfiles[i])
		printf("\t$(CC) $(CFLAGS) -c -o $(@) $(<)\n\n")
	}

	for (i in sfiles) {
		printf("%s: %s\n", sobjs[i], sfiles[i])
		printf("\t$(CC) $(CFLAGS) -c -o $(@) $(<)\n\n")
	}
}
